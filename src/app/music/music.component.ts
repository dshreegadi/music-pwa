import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.css']
})
export class MusicComponent implements OnInit {

  constructor(
    private _apiService: ApiService
  ) { }

  ngOnInit() {
    this.getMusic();
  }

  musicList = [];
  getMusic() {
    this._apiService.getMusic().subscribe(
      (res) => {
        this.musicList = res['result'];
      }, (err) => { }
    );
  }

}
