export class CONSTANTS {
    public static CLIENT_ID = '0f25b089-38cb-47d2-bcdc-99a2a6a8fe1b'; //Application (client) ID
    public static REDIRECT_URI = 'http://localhost:4200/';
    public static AUTHORITY = 'https://login.microsoftonline.com/common'; //Authority
}