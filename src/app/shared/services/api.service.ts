import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONSTANTS } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  getMusic() {
    const BASE_URL = 'https://orion.apiseeds.com/api/music/search/?q=Numb&apikey=cuxcMDqEkN92I1OLsnaykdXaVUyM1gBoa6lqZrPivKmAMbm7egA7YKTnUeMmu5h4&limit=25';
    return this.http.get(BASE_URL);
  }
}
