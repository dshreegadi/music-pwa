import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONSTANTS } from '../shared/globals';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = null;

    token = localStorage.getItem("token");

    req = req.clone({
      setHeaders: {
        'Authorization': 'Bearer ' + '9d016e0dd0e34230874ba8cd2d097b81'
      }
    });
    return next.handle(req);
  }

}